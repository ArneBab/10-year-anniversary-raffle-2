Want to come meet the Bitbucket team in San Francisco and celebrate Atlassian's new SOMA home and 10 Year Anniversary? We're having a raise-the-roof party featuring live music, drinks, delicious treats, and more! 

* When: Friday, April 13th @ 5:30pm – 10:30pm
* Where: 1098 Harrison Street, San Francisco

If you're interested in being a part of this euphoric evening, fork this repo, answer some basic questions on how you think we can make Bitbucket better and then issue a pull request. First 50 to respond will receive an invite to the party.

In addition to attending our awesome party, you'll receive a free Bitbucket t-shirt in your preferred size.